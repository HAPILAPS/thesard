Thésard
=======
Thésard est une application web de rencontre entre étudiants en quête d'un thèse d'exercice et directeurs de thèse.

## Licence

Le logiciel Thésard est sous licence AGPL version 3 et ultérieures.
